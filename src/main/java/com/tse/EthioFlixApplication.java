package com.tse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EthioFlixApplication {

	public static void main(String[] args) {
		SpringApplication.run(EthioFlixApplication.class, args);
	}

}


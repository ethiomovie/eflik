package com.ab.ethioflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EflixApplication {

	public static void main(String[] args) {
		SpringApplication.run(EflixApplication.class, args);
	}

}


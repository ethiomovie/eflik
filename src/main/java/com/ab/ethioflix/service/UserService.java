package com.ab.ethioflix.service;

import com.ab.ethioflix.model.User;

public interface UserService {
	  
	 public User findUserByEmail(String email);
	 
	 public void saveUser(User user);
}